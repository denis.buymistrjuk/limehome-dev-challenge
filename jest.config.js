module.exports = {
  preset: 'ts-jest',
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.json'
    }
  },
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },
  testEnvironment: 'node',
  testMatch: [
    '<rootDir>/tests/**/*.{test,spec}.ts',
    '<rootDir>/tests/*.{test,spec}.ts',
  ],
  moduleFileExtensions: [
    'ts',
    'js',
    'json',
  ],
  setupFilesAfterEnv: ['jest-extended', '<rootDir>/tests/jest-setup.ts'],
  resetModules: false,
  collectCoverage: true,
  coverageDirectory: './',
  collectCoverageFrom: [
    '**/*.{ts,dts}',
    '!**/node_modules/**',
    '!**/tests/**'
  ],
  coverageReporters: ['lcov', 'text'],
  coveragePathIgnorePatterns: [
    '/dist/'
  ]
};
