import axios, { AxiosResponse } from 'axios';
import config from '../../config';
import { HereAPICategoriesEnum, IAccommodation } from './types';

const HERE_API_URL = 'https://places.ls.hereapi.com/places/v1/browse';

/**
 *
 * @param {{[p:string]:string}} params
 *
 * @private
 * @return {Promise<AxiosResponse>}
 */
async function callHereAPI (params:{ [name: string]: string }):Promise<AxiosResponse> {
  return await axios.get(HERE_API_URL, {
    params: Object.assign({}, params, {
      app_id: config.here_places_api.app_id,
      apiKey: config.here_places_api.api_key
    })
  });
}

/**
 *
 * @param {string} location
 *
 * @return {Promise<IAccommodation[] | []>}
 */
export async function getAccommodationAroundLocation (location:string):Promise<IAccommodation[] | []> {
  const resp:AxiosResponse = await callHereAPI({
    at: location,
    cat: HereAPICategoriesEnum.Accommodation
  });

  let data:IAccommodation[] | [] = [];
  if (resp.data && resp.data.results && resp.data.results.items && resp.data.results.items.length) {
    data = resp.data.results.items;
  }

  return data;
}
