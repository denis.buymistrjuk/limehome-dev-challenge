export enum HereAPICategoriesEnum {
  EatDrink = 'eat-drink',
  Restaurant = 'Restaurant',
  CoffeeTea = 'coffee-tea',
  SnacksFastFood = 'snacks-fast-food',
  GoingOut = 'going-out',
  SightsMuseums = 'sights-museums',
  Transport = 'transport',
  Airport = 'airport',
  Accommodation = 'accommodation',
  Shopping = 'shopping',
  LeisureOutdoor = 'leisure-outdoor',
  AdministrativeAreasBuildings = 'administrative-areas-buildings',
  NaturalGeographical = 'natural-geographical',
  PetrolStation = 'petrol-station',
  AtmBankExchange = 'atm-bank-exchange',
  ToiletRestArea = 'toilet-rest-area',
  HospitalHealthCareFacility = 'hospital-health-care-facility'
}

export interface IAccommodationCategory {
  id: string;
  title: string;
  href: string;
  type: string;
  system: string;
}

export interface IAccommodation {
  position: number[];
  distance: number;
  title: string;
  averageRating: number;
  category: IAccommodationCategory;
  icon: string;
  vicinity: string;
  having: string[];
  type: string;
  href: string;
  id: string;
}