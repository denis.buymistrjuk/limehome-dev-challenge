import { ObjectSchema } from 'joi';

export async function requestValidation(data:object, $joiSchema:ObjectSchema) {
  try {
    await $joiSchema.validateAsync(data)
  } catch (e) {
    throw new Error(e.message)
  }
}