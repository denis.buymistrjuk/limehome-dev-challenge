import { APIGatewayProxyResult } from 'aws-lambda';
import { ValidationError } from 'joi';

export class LambdaResponses {
  /**
   * Response headers
   *
   * @type {{[p:string]:boolean | number | string}}
   * @private
   */
  private static _headers:{ [header: string]: boolean | number | string } = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': true,
  }

  /**
   * Success response
   *
   * @param {object} data
   *
   * @return {APIGatewayProxyResult}
   */
  public static success(data: object): APIGatewayProxyResult {
    return {
      statusCode: 200,
      headers: LambdaResponses._headers,
      body: JSON.stringify(data)
    }
  }

  /**
   * Server error
   *
   * @param {Error | string} err
   * @param {number} statusCode
   *
   * @return {APIGatewayProxyResult}
   */
  public static failure(err:Error|string, statusCode:number = 500): APIGatewayProxyResult {
    const error = err instanceof Error ? err.message : err;

    return {
      statusCode,
      headers: LambdaResponses._headers,
      body: JSON.stringify({ error }),
    }
  }

  /**
   * Bad request
   *
   * @param {Joi.ValidationError | Error | string} err
   *
   * @return {APIGatewayProxyResult}
   */
  public static badRequest(err:ValidationError|Error|string): APIGatewayProxyResult {
    return LambdaResponses.failure(err, 400);
  }
}
