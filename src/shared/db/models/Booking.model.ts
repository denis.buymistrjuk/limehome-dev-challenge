import { Sequelize, Model, DataTypes } from 'sequelize';

export class Booking extends Model {
  public id!: number;
  public propertyId!: string;
  public name!: string;
  public category!: string|null;
  public description!: string|null;
  public location!:{ lat: number, lon: number };
  public address!: string;
  public from!: Date;
  public to!: Date;
}

export class BookingFactory {
  /**
   *
   * @type {string}
   * @private
   */
  private static _tableName = 'bookings';

  /**
   *
   * @type {Object}
   * @private
   */
  private static _ormAttributes = {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    propertyId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    category: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    description: {
      type: DataTypes.STRING(256),
      allowNull: true,
    },
    location: {
      type: DataTypes.JSON,
      allowNull: false
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    from: {
      type: DataTypes.DATE,
      allowNull: false
    },
    to: {
      type: DataTypes.DATE,
      allowNull: false
    }
  };

  /**
   *
   * @param {Sequelize} sequelize
   *
   * @return {Booking}
   */
  public static initModel(sequelize:Sequelize) {
    Booking.init(BookingFactory._ormAttributes, {
      tableName: BookingFactory._tableName,
      sequelize: sequelize
    });

    return Booking;
  }
}
