import { Sequelize } from 'sequelize';

import { BookingFactory, Booking } from './models/Booking.model';
import config from '../config';

const sequelize = new Sequelize(config.db.name, config.db.username, config.db.password, {
  dialect: 'postgres',
  host: config.db.host,
  port: +config.db.port,
  logging: false,
  ssl: process.env.NODE_ENV !== 'local'
});

BookingFactory.initModel(sequelize);

export { Booking };

export class DBConnection {
  /**
   * Store the connection state
   *
   * @type {boolean}
   */
  public static isConnected:boolean = false;

  /**
   * Test connection established and sync models
   *
   * @param {boolean} force
   *
   * @return {Promise<Sequelize>}
   */
  public static async init(force?:boolean):Promise<Sequelize> {
    try {
      if (force || !DBConnection.isConnected) {
        await sequelize.authenticate();
        await sequelize.sync();
        DBConnection.isConnected = true;
      }

      console.log('\n[DBCONNECTION.ISCONNECTED]', DBConnection.isConnected);

      return sequelize;
    } catch (e) {
      DBConnection.isConnected = false;
      throw e;
    }
  }

  /**
   * Close DB connection
   *
   * @return {Promise<void>}
   */
  public static async close():Promise<void> {
    await sequelize.close();
    DBConnection.isConnected = false;
  }
}

