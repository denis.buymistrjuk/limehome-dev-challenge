import * as dotenv from 'dotenv';
if (!process.env.NODE_ENV || process.env.NODE_ENV === 'local') {
  dotenv.config({ path: __dirname + '/../../.env' });
}

export default {
  db: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT || 5432,
    name: process.env.DB_NAME,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
  },
  here_places_api: {
    app_id: process.env.HERE_PLACES_APP_ID,
    api_key: process.env.HERE_PLACES_API_KEY
  }
}