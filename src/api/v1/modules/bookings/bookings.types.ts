import Joi from 'joi';

export interface ICreateBooking {
  propertyId: string;
  name: string;
  location:{ lat:number, lon:number };
  address: string;
  from: Date;
  to: Date;
  category?: string|null;
  description?: string|null;
}

export const $createBookingJoiSchema = Joi.object({
  propertyId: Joi.string().required(),
  name: Joi.string().required(),
  category: Joi.string().optional(),
  description: Joi.string().optional(),
  location: Joi.object().keys({
    lat: Joi.number().required(),
    lon: Joi.number().required(),
  }).required(),
  address: Joi.string().required(),
  from: Joi.date().iso().required(),
  to: Joi.date().iso().required()
});

export const $getPropertyBookingsSchema = Joi.object({
  propertyId: Joi.string().min(1).required()
})
