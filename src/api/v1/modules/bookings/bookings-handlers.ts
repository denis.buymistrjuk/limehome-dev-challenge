import { APIGatewayEvent, APIGatewayProxyResult } from 'aws-lambda';

import { LambdaResponses } from '../../../../shared/utils/lambdaResponses';
import { DBConnection, Booking } from '../../../../shared/db';
import { requestValidation } from '../../../../shared/utils/validation';
import { BookingsService } from './bookings.service';
import { ICreateBooking, $createBookingJoiSchema, $getPropertyBookingsSchema } from './bookings.types';

/**
 * Handler: Create booking
 *
 * @param {APIGatewayEvent} event
 *
 * @return {Promise<APIGatewayProxyResult>}
 */
export const createBookingHandler = async (event:APIGatewayEvent):Promise<APIGatewayProxyResult> => {
  let inputData:ICreateBooking;

  await DBConnection.init();

  try {
    inputData = JSON.parse(event.body);
  } catch (e) {
    console.log('[ERROR][BAD_REQUEST]', 'incorrect', e);
    return LambdaResponses.failure('Incorrect request body data', 400);
  }

  try {
    await requestValidation(inputData, $createBookingJoiSchema);
  } catch (e) {
    console.log('[ERROR][BAD_REQUEST]', 'validation', e);
    return LambdaResponses.badRequest(e);
  }

  try {
    const $bookingDoc:Booking = await BookingsService.createBooking(inputData);

    return LambdaResponses.success({
      // note: good to have DTO here
      data: $bookingDoc
    });
  } catch (e) {
    console.log('[ERROR][FATAL]', e);
    return LambdaResponses.failure(e);
  }
}

/**
 * Handler: Get bookings list for particular property
 *
 * @param {APIGatewayEvent} event
 *
 * @return {Promise<APIGatewayProxyResult>}
 */
export const getPropertyBookingsHandler = async (event:APIGatewayEvent):Promise<APIGatewayProxyResult> => {
  try {
    await requestValidation(event.pathParameters, $getPropertyBookingsSchema);
  } catch (e) {
    console.log('[ERROR][BAD_REQUEST]', 'validation', e);
    return LambdaResponses.badRequest(e);
  }

  const bookings:Booking[] | [] = await BookingsService.getPropertyBookings(event.pathParameters.propertyId);

  return LambdaResponses.success({
    // note: good to have DTO here
    data: bookings
  });
}