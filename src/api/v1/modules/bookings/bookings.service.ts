import { Booking } from '../../../../shared/db';
import { ICreateBooking } from './bookings.types';

export class BookingsService {
  /**
   * Create booking
   *
   * @param {ICreateBooking} data
   *
   * @return {Promise<Booking>}
   */
  public static async createBooking (data:ICreateBooking) {
    return await Booking.create(data)
  }

  /**
   * Get bookings list by property
   *
   * @param {string} propertyId
   *
   * @return {Promise<Booking[]>}
   */
  public static async getPropertyBookings (propertyId:string):Promise<Booking[]> {
    return await Booking.findAll({ where: { propertyId }});
  }
}