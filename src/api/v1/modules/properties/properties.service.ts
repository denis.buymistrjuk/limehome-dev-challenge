import { IGetProperties } from './properties.types';
import { getAccommodationAroundLocation } from '../../../../shared/services/hereAPI';

export class PropertiesService {
  /**
   * Get properties by location
   *
   * @param {IGetProperties} loc
   *
   * @return {Promise<any>}
   */
  public static getPropertiesByLocation (loc:IGetProperties) {
    const location = [loc.lat, loc.lon].join(',');

    return getAccommodationAroundLocation(location);
  }
}