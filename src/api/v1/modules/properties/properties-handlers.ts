import { APIGatewayEvent, APIGatewayProxyResult } from 'aws-lambda';
import { LambdaResponses } from '../../../../shared/utils/lambdaResponses';
import { IGetProperties, $getPropertiesQuery } from './properties.types';
import { requestValidation } from '../../../../shared/utils/validation';
import { PropertiesService } from './properties.service';

/**
 * Handler: get properties list based on the location
 *
 * @param {APIGatewayEvent} event
 *
 * @return {Promise<APIGatewayProxyResult>}
 */
export const getPropertiesHandler = async (event:APIGatewayEvent):Promise<APIGatewayProxyResult> => {
  if (!event.queryStringParameters || !event.queryStringParameters.at) {
    return LambdaResponses.badRequest(new Error('Parameter "at" is required'));
  }

  // FIXME: problem with empty value
  const [ lat, lon ] = event.queryStringParameters.at.split(',');
  const inputData:IGetProperties = { lat: +lat, lon: +lon };

  try {
    await requestValidation(inputData, $getPropertiesQuery);
  } catch (e) {
    console.log('[ERROR][BAD_REQUEST]', 'validation', e);
    return LambdaResponses.badRequest(e);
  }

  try {
    const properties = await PropertiesService.getPropertiesByLocation(inputData);

    return LambdaResponses.success({
      // note: good to have DTO here
      data: properties
    });
  } catch (e) {
    console.log('[ERROR][FATAL]', e);
    return LambdaResponses.failure(e);
  }
}
