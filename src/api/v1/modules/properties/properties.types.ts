import Joi from 'joi';

export interface IGetProperties {
  lat: number;
  lon: number;
}

export const $getPropertiesQuery = Joi.object({
  lat: Joi.number().required(),
  lon: Joi.number().required()
});