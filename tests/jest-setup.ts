// import 'jest';
jest.setTimeout(10000);

process.env.STAGE = 'testing';
process.env.region = 'eu-west-1';
process.env.DB_HOST = 'limehome-integration-tests.cs8laez4dsyj.eu-west-1.rds.amazonaws.com';
process.env.DB_PORT = '5432';
process.env.DB_NAME = 'limehome-integration-tests'
process.env.DB_USERNAME = 'postgres'
process.env.DB_PASSWORD = 'v1v2v3v4v5'
process.env.HERE_PLACES_APP_ID = '1'
process.env.HERE_PLACES_API_KEY = '2'