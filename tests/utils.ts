import {
  APIGatewayEvent,
  APIGatewayEventRequestContextWithAuthorizer,
  APIGatewayEventDefaultAuthorizerContext
} from 'aws-lambda';

import { DBConnection, Booking } from '../src/shared/db';

type KeyValParams = { [name: string]: string };

class FakeAPIGatewayEvent implements APIGatewayEvent {
  body: string | null;
  headers: { [name: string]: string };
  pathParameters: { [name: string]: string } | null;
  queryStringParameters: { [name: string]: string } | null;
  httpMethod:string;
  isBase64Encoded:boolean;
  multiValueHeaders:{ [p:string]:string[] };
  multiValueQueryStringParameters:{ [p:string]:string[] } | null;
  path:string;
  requestContext:APIGatewayEventRequestContextWithAuthorizer<APIGatewayEventDefaultAuthorizerContext>;
  resource:string;
  stageVariables:{ [p:string]:string } | null;

  constructor(event:APIGatewayEvent|object = {}) {
    Object.assign(this, event);
  }

  setHeaders(headers:KeyValParams): this {
    this.headers = headers;
    return this;
  }

  setParams(params:KeyValParams): this {
    this.pathParameters = params;
    return this;
  }

  setQueryParams(query:KeyValParams): this {
    this.queryStringParameters = query;
    return this;
  }

  setBody(body: any): this {
    this.body = typeof body === 'string' ? body : JSON.stringify(body);

    return this;
  }
}

export const createFakeAPIGatewayEvent = (
  httpMethod: string,
  urlPath: string,
  params: KeyValParams | null = null,
  query: KeyValParams | null = null,
  body: KeyValParams | null = null,
):FakeAPIGatewayEvent => {
  const fakeEvent = new FakeAPIGatewayEvent({
    httpMethod: urlPath,
    resource: urlPath,
    requestContext: {
      resourceId: '123456',
      apiId: '1234567890',
      resourcePath: urlPath,
      httpMethod: httpMethod.toUpperCase(),
      requestId: 'c6af9ac6-7b61-11e6-9a41-93e8deadbeef',
      accountId: '123456789012',
      stage: 'Stage',
      identity: {
        apiKey: null,
        userArn: null,
        cognitoAuthenticationType: null,
        caller: null,
        userAgent: 'Custom User Agent String',
        user: null,
        cognitoIdentityPoolId: null,
        cognitoAuthenticationProvider: null,
        sourceIp: '127.0.0.1',
        accountId: null
      },
      extendedRequestId: null,
      path: urlPath
    },
    pathParameters: null,
    queryStringParameters: null,
    multiValueQueryStringParameters: null,
    body: null,
    headers: {
      'Host': '127.0.0.1:3000',
      'User-Agent': 'curl/7.54.0',
      'Accept': '*/*',
      'Content-Type': 'application/json',
      'Content-Length': '132',
      'X-Forwarded-Proto': 'http',
      'X-Forwarded-Port': '3000'
    },
    multiValueHeaders: {
      'Host': ['127.0.0.1:3000'],
      'User-Agent': ['curl/7.54.0'],
      'Accept': ['*/*'],
      'Content-Type': ['application/json'],
      'Content-Length': ['132'],
      'X-Forwarded-Proto': ['http'],
      'X-Forwarded-Port': ['3000']
    },
    stageVariables: null,
    path: urlPath,
    isBase64Encoded: false
  });

  params && fakeEvent.setParams(params)
  query && fakeEvent.setQueryParams(query)
  body && fakeEvent.setBody(body)

  return fakeEvent;
}

export const PROPERTY_ID = '804u8c56-cd9b3093661f4b4e8f524f7e92c23dac';
export const MOCK_BOOKINGS = {
  FIRST: {
    propertyId: PROPERTY_ID,
    name: 'hotel 1',
    location: {
      lat: 49.839684,
      lon: 24.029716
    },
    address: 'hotel 1 address',
    from: new Date(),
    to: new Date(),
  },
  SECOND: {
    propertyId: PROPERTY_ID,
    name: 'hotel 2',
    location: {
      lat: 49.839684,
      lon: 24.029716
    },
    address: 'hotel 2 address',
    from: new Date(),
    to: new Date(),
  },
  THIRD: {
    propertyId: PROPERTY_ID,
    name: 'hotel 3',
    location: {
      lat: 49.839684,
      lon: 24.029716
    },
    address: 'hotel 3 address',
    from: new Date(),
    to: new Date(),
  }
};

export const clearDb = async ():Promise<void> => {
  await DBConnection.init();
  await Booking.destroy({ where: {}, truncate: true });
}

export const populateDb = async ():Promise<Booking[]> => {
  await DBConnection.init();

  return await Promise.all([
    Booking.create(MOCK_BOOKINGS.FIRST),
    Booking.create(MOCK_BOOKINGS.SECOND),
    Booking.create(MOCK_BOOKINGS.THIRD)
  ]);
}