import { createFakeAPIGatewayEvent, clearDb, populateDb } from '../utils';
import { createBookingHandler } from '../../src/api/v1/modules/bookings/bookings-handlers';

describe('Handler: Create Booking', () => {

  beforeAll(async () => {
    await clearDb();
    await populateDb();
  });

  it('Should fail without parameters', async () => {
    const event = createFakeAPIGatewayEvent('POST', '/api/bookings')
    const result = await createBookingHandler(event);

    expect(result).toHaveProperty('statusCode', 400);
    expect(result).toHaveProperty('headers');
    expect(result).toHaveProperty('body');
  });

  it('Should fail without required parameters', async () => {
    const data = {
      name: 'test',
      location: {
        lat: 32.33,
        lon: 18.22
      },
      address: 'some address',
      from: new Date(),
      to: new Date()
    };
    const event = createFakeAPIGatewayEvent('POST', '/api/bookings').setBody(data);
    const result = await createBookingHandler(event);

    expect(result).toHaveProperty('statusCode', 400);
    expect(result).toHaveProperty('headers');
    expect(result).toHaveProperty('body');

    const body = JSON.parse(result.body);
    expect(body).toHaveProperty('error', '"propertyId" is required');
  });

  it('Should fail with broken input data (JSON)', async () => {
    const event = createFakeAPIGatewayEvent('POST', '/api/bookings')
      .setBody('broken_data');
    const result = await createBookingHandler(event);

    expect(result).toHaveProperty('statusCode', 400);
    expect(result).toHaveProperty('headers');
    expect(result).toHaveProperty('body');

    const body = JSON.parse(result.body);
    expect(body).toHaveProperty('error', 'Incorrect request body data');
  });

  it('Should successfully create booking', async () => {
    const data:any = {
      propertyId: 'somePropId1w2',
      name: 'hotel',
      category: 'hotel',
      description: 'hotel description',
      location: {
        lat: 32.33,
        lon: 18.22
      },
      address: 'some hotel address',
      from: new Date(),
      to: new Date()
    };
    const event = createFakeAPIGatewayEvent('POST', '/api/bookings').setBody(data);
    const result = await createBookingHandler(event);

    expect(result).toHaveProperty('statusCode', 200);
    expect(result).toHaveProperty('headers');
    expect(result).toHaveProperty('body');

    const body = JSON.parse(result.body);
    expect(body).toHaveProperty('data');

    Object.keys(data).map((key) => {
      let value = data[key];
      if (['from', 'to'].includes(key)) {
        value = value.toISOString();
      }
      expect(body.data).toHaveProperty(key, value);
    });
    expect(body.data).toHaveProperty('createdAt');
    expect(body.data).toHaveProperty('updatedAt');
  });

});