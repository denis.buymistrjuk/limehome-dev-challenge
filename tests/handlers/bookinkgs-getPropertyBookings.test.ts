import { createFakeAPIGatewayEvent, clearDb, populateDb, PROPERTY_ID, MOCK_BOOKINGS } from '../utils';
import { getPropertyBookingsHandler } from '../../src/api/v1/modules/bookings/bookings-handlers';

describe('Handler: Get bookings for a property', () => {
  let populatedData = [];
  beforeAll(async () => {
    await clearDb();
    await populateDb();
  });

  it('Should fail without parameters', async () => {
    const propertyId = '';
    const event = createFakeAPIGatewayEvent(
      'POST',
      `/api/properties/${propertyId}/bookings`
    ).setParams({});
    const result = await getPropertyBookingsHandler(event);

    expect(result).toHaveProperty('statusCode', 400);
    expect(result).toHaveProperty('headers');
    expect(result).toHaveProperty('body');

    const body = JSON.parse(result.body);
    expect(body).toHaveProperty('error', '"propertyId" is required');
  });

  it('Should return an empty list with unknown propertyId', async () => {
    const propertyId = '[unknown_property_id]';
    const event = createFakeAPIGatewayEvent(
      'POST',
      `/api/properties/${propertyId}/bookings`
    ).setParams({ propertyId });
    const result = await getPropertyBookingsHandler(event);

    expect(result).toHaveProperty('statusCode', 200);
    expect(result).toHaveProperty('headers');
    expect(result).toHaveProperty('body');

    const body = JSON.parse(result.body);
    expect(body).toHaveProperty('data');
    expect(body.data).toHaveLength(0);
  });

  it('Should return list with bookings by propertyID', async () => {
    const propertyId = PROPERTY_ID;
    const event = createFakeAPIGatewayEvent(
      'POST',
      `/api/properties/${propertyId}/bookings`
    ).setParams({ propertyId });
    const result = await getPropertyBookingsHandler(event);

    expect(result).toHaveProperty('statusCode', 200);
    expect(result).toHaveProperty('headers');
    expect(result).toHaveProperty('body');

    const body = JSON.parse(result.body);
    expect(body).toHaveProperty('data');
    expect(body.data).toHaveLength(Object.keys(MOCK_BOOKINGS).length);

    const data:any = MOCK_BOOKINGS.FIRST;
    Object.keys(data).map((key) => {
      let value = data[key];
      if (['from', 'to'].includes(key)) {
        value = value.toISOString();
      }
      expect(body.data[0]).toHaveProperty(key, value);
    });
  })
});