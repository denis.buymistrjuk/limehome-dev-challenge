import { createFakeAPIGatewayEvent, clearDb, populateDb } from '../utils';
import { getPropertiesHandler } from '../../src/api/v1/modules/properties/properties-handlers';

const mockData = require('../data/accommodations.json');
jest.mock('axios', () => ({
  default: {
    get: jest.fn().mockImplementation(() => Promise.resolve({
      data: {
        results: { items: mockData }
      }
    })),
  },
  get: jest.fn(() => Promise.resolve({
    data: {
      results: { items: mockData }
    }
  })),
}));

describe('Handler: Get properties list', () => {
  beforeAll(async () => {
    jest.restoreAllMocks();
    await clearDb();
    await populateDb();
  });

  it('Should fail without parameters', async () => {
    const event = createFakeAPIGatewayEvent('GET', '/api/properties')
    const result = await getPropertiesHandler(event);

    expect(result).toHaveProperty('statusCode', 400);
    expect(result).toHaveProperty('headers');
    expect(result).toHaveProperty('body');
  });

  it('Should fail with wrong parameters', async () => {
    const event = createFakeAPIGatewayEvent('GET', '/api/properties')
      .setQueryParams({at: 'wrong_parameter'});
    const result = await getPropertiesHandler(event);

    expect(result).toHaveProperty('statusCode', 400);
    expect(result).toHaveProperty('headers');
    expect(result).toHaveProperty('body');
  });

  it('Should fail with wrong parameters (non number)', async () => {
    const event = createFakeAPIGatewayEvent('GET', '/api/properties')
      .setQueryParams({at: 'first,second'});
    const result = await getPropertiesHandler(event);

    expect(result).toHaveProperty('statusCode', 400);
    expect(result).toHaveProperty('headers');
    expect(result).toHaveProperty('body');
  });

  it('Should successfully return data', async () => {
    const event = createFakeAPIGatewayEvent('GET', '/api/properties')
      .setQueryParams({at: '49.839684,24.029716'});
    const result = await getPropertiesHandler(event);



    expect(result).toHaveProperty('statusCode', 200);
    expect(result).toHaveProperty('headers');
    expect(result).toHaveProperty('body');


    const body = JSON.parse(result.body);
    expect(body).toHaveProperty('data');
    expect(body.data).toHaveLength(mockData.length);
  });
});