# Limehome Test Task
Technologies:
- Node.js v12
- TypeScript
- PostgreSQL
- Serverless framework

# Install
```bash
git clone git@gitlab.com:denis.buymistrjuk/limehome-dev-challenge.git
```
```bash
npm install
```
```bash
npm intall -g serverless
```
```bash
npm intall -g serverless-offline
```

# Run locally
First thing first you need to set env variables in .env file. (.env.example provided), and then run:
```bash
npm run start:local
```
# APIDoc
Result endpoint: [https://a7k6evnflk.execute-api.eu-west-1.amazonaws.com/production/](https://a7k6evnflk.execute-api.eu-west-1.amazonaws.com/production/)

| Lambdas                    | Path                                        | Description                           |
| -------------------------- | ------------------------------------------- | --------------------------------------|
| getPropertiesHandler       | [GET] /api/properties?at=lat,lon            | Returns the property around Lat/Lon   |
| createBookingHandler       | [POST] /api/bookings                        | Creates a booking for a property.     |
| getPropertyBookingsHandler | [GET] /api/properties/{propertyId}/bookings | Returns the bookings for a property   |

# Tests
The tests have been done using jest, there are integration test coverage.
The coverage graph is available in a folder "Icon-report" and after tests have been finished.
```bash
npm run test:integration
```